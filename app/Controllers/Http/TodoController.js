'use strict'

const Todo = use('App/Models/Todo')

class TodoController {
    async index ({request, response, view})
    {
        const todos = await Todo.all()
        return view.render('todo.index', {todos:todos.toJSON()})
    }

    async store ({request, response})
    {
        const req = await request.all()

        const todo = await new Todo()
        todo.content  =  req.content
        todo.save()

        response.redirect('/', true)
    }
}

module.exports = TodoController
