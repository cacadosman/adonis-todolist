'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Todo extends Model {
    static get visible () 
    {
        return ['content']
    }
}

module.exports = Todo
